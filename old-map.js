// Calculando distancia
function calcularDistancias(latitudePosto, longitudePosto, latitudeOrigem, longitudeOrigem) {
    let posto = [latitudePosto, longitudePosto];
    // console.log(posto);
    let origem = [latitudeOrigem, longitudeOrigem];
    // console.log(origem);
    let service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            //Origem
            origins: [origem],
            //Destino
            destinations: [posto],
            //Modo (DRIVING | WALKING | BICYCLING)
            travelMode: google.maps.TravelMode.DRIVING,
            //Sistema de medida (METRIC | IMPERIAL)
            unitSystem: google.maps.UnitSystem.METRIC
            //Vai chamar o callback
        });
}

// Tratar o retorno do DistanceMatrixService
function callback(response, status) {
    //Verificar o Status
    if (status != google.maps.DistanceMatrixStatus.OK) {
        //Se o status não for "OK"
        //     $('#litResultado').html(status);
        console.log('Deu ruim:' + status);
    } else {
        console.log('Deu bom!');
        //Se o status for OK
        //Endereço de origem = response.originAddresses
        //Endereço de destino = response.destinationAddresses
        //Distância = response.rows[0].elements[0].distance.text
        // //Duração = response.rows[0].elements[0].duration.text
        // console.log('Origem: ' + response.originAddresses);
        // console.log('Destino: ' + response.destinationAddresses);
        // console.log(response);
        // $('#litResultado').html("<strong>Origem</strong>: " + response.originAddresses +
        //     "<br /><strong>Destino:</strong> " + response.destinationAddresses +
        //     "<br /><strong>Distância</strong>: " + response.rows[0].elements[0].distance.text +
        //     " <br /><strong>Duração</strong>: " + response.rows[0].elements[0].duration.text
        // );
//Atualizar o mapa
//         $("#map").attr("src", "https://maps.google.com/maps?saddr=" + response.originAddresses + "&daddr=" + response.destinationAddresses + "&output=embed");
//     }
    }
}

//Pegando latitude e longitude do input
function codeAddress(latitudePosto, longitudePosto) {
    let address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            let latitudeOrigem = (results[0].geometry.location.lat());
            let longitudeOrigem = (results[0].geometry.location.lng());
            funcaoUsandoVariaveis(latitudeOrigem, longitudeOrigem);
            // calcularDistancias(latitudePosto, longitudePosto, latitudeOrigem, longitudeOrigem);

            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function initMap(lat, long) {
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: long},
        zoom: 15
    });
}

let map;

function initMap(lat, long) {
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: long},
        zoom: 15
    });
}

//Calcular quantos kms o carro pode fazer
//Esses dados são solicitados ao usuario em kms
let inputQuantCombustivel = document.getElementById('quantCombustivel');
let inputKmsPorLitro = document.getElementById('kmsporlitro');

function autonomia() {
    let quantCombustivel = inputQuantCombustivel.value;
    let kmporlitro = inputKmsPorLitro.value;
    let resultAutonomia = kmporlitro * quantCombustivel;
    codeAddress(resultAutonomia);
    return resultAutonomia;
}

//Pegando latitude e longitude do input
function codeAddress(autonomia) {
    let address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            let latitudeOrigem = (results[0].geometry.location.lat());
            let longitudeOrigem = (results[0].geometry.location.lng());
            let localAtual = address.split(' ').join('+');
            getLatLongOrigem(localAtual, latitudeOrigem, longitudeOrigem, autonomia);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

//recebendo a latitude e longitude do input
//fazendo request dos postos
//retornando os postos
function getLatLongOrigem(localAtual, latitudeOrigem, longitudeOrigem, autonomia) {
    let resultPag1 = axios.get(`https://maps.googleapis.com/maps/api/place/textsearch/json?&sensor=true&location=${latitudeOrigem},${longitudeOrigem}&rankby=distance&type=gas_station&key=AIzaSyCWE8rjSkJ-PraX025-aNNWS3ayhLC2GHg`);

    let resultPag2 = axios.get(`https://maps.googleapis.com/maps/api/place/textsearch/json?&sensor=true&location=${latitudeOrigem},${longitudeOrigem}&rankby=distance&opennow=true&type=gas_station&key=AIzaSyCWE8rjSkJ-PraX025-aNNWS3ayhLC2GHg&pagetoken=CrQCJAEAAKA-duIa3YOyOnW3v-xeZloZvzINXtKzXCtIXdHG8IlO40RiSEABHP34M_tSep5zyadYywY431OkNe2yYC5y8dGiaC7ATLVcYNPVs1v_sBjYbp57ButDRgV_4P9dS2TNYc2tKUidgfVasq_sTPdhCSwk-OM5gH_MFAmkRGqbRlW_NsBUpXkszb_Qi_eYgPrsOLA-l-DdtdMTx6YpwlvfYCDN23IRlUaTpsavPMZm_rpKTHFiwbtQqQztoeBWquGtx-sZmmI3GaxaCcThhKHHOQ_-ZSljWJoUfXf9KcbvlIxTB06sFsvZ5os0k5dSGpboFCOCxZJdV9lB32O1hUgq-bXJKM2IN1sUJPBmoSZcJ67527mkquFcMo_qIsgpNrf--w4Auzkufzp069M18NHGOkwSEAqRqCw3s4pcLp2pJTTwmisaFNXp7qJM0j_Qhz3-HUbl4YguCwCb`);

    let resultPag3 = axios.get(`https://maps.googleapis.com/maps/api/place/textsearch/json?&sensor=true&location=${latitudeOrigem},${longitudeOrigem}&rankby=distance&opennow=true&type=gas_station&key=AIzaSyCWE8rjSkJ-PraX025-aNNWS3ayhLC2GHg&pagetoken=CtQDxAEAAMR5kEiYteszH5dX0XKm2Y6q2ZcsPkkiR-tovxM_QAi2HJIydV8FcOsFPt46x2lHSJmucHdpNZtiF64wq1ujQACX4sLYrEtPjBWps-KweX3RCyPlSWEMiunr1GiUNBEvdZlRMAtd9B7IFSG_4OW8BJySCDQUDKj2eLok82H7wfYHqtK9nkV0dpBFprq0oOKSvkBVoNv84xEc61l7pns8jLvpuIDLn13VfTZIXZaWGh-LfPu9S0VhN3m3xOsGVhUZyBjWvjSByMD7Ti1ScqSOIrcddzcIO_XwQ6EAf0ST3n6l-fh-Iy_-XkO1IYoX-gBiB94wvDxb3ELpoEDo1phxLcuHYPVUElIgyNBydS4MfNsHelAOLbV3pxK0dG6fxo6cZ8ZBodTT6-lRPuAEazMh-2OheEda3tkP3VsharwkGe___-iRBsP9p1oeKsztTpDRQsUqGkBTiqtPUE8v5fo4T7mCnpiAu3gsJAFTEIGJMhYujoZaDYCHqAJv1wkczuBuq27ruKKtPIGM-LYA1EINGaX6EqcRUzBxYZU6IBESWuZwlAplv0dMOFDB6DxJYX2pcdLW_FskYDAesvAWxLcyBYgJNJCK_De-1ks3Tj2kI25TEhChj-lXC0FpfgmpTB_t1mVeGhRPaiHL-LO7iqJKq-W0LiKWxQ7sFw`);
    axios.all([resultPag1, resultPag2, resultPag3])
        .then(axios.spread(function (postos1Response, postos2Response, postos3Response) {
            let resultPostos1 = postos1Response.data.results;
            let arrayPostos = resultPostos1.map(item => item.name + '+Mossoro+RN');
            console.log(arrayPostos);
            let postosConcatenados1 = arrayPostos.join('|').split(' ').join('+');
            //Request 2
            let resultPostos2 = postos2Response.data.results;
            let arrayPostos2 = resultPostos2.map(item => item.name + '+Mossoro+RN');
            let postosConcatenados2 = arrayPostos2.join('|').split(' ').join('+');
            // Request 3
            let resultPostos3 = postos3Response.data.results;
            let arrayPostos3 = resultPostos3.map(item => item.name + '+Mossoro+RN');
            let postosConcatenados3 = arrayPostos3.join('|').split(' ').join('+');
            let todosPostosConcatenados = postosConcatenados1 + '+' + postosConcatenados2 + '+' + postosConcatenados3;
            getDistancia(localAtual, todosPostosConcatenados, autonomia);
        }))
        .catch((erro) => alert(erro));
}

//Recebe endereco de origem e enderecos de destino
//Pegando Distancia
function getDistancia(enderecoOrigem, enderecosDestinos, autonomia) {
    let requestDistancia = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${enderecoOrigem}&destinations=${enderecosDestinos}&departure_time=now&key=AIzaSyBd5ZlMTTGfdmYEC5QiGULz1uJEo98if-c`;
    axios.get(requestDistancia)
        .then((response) => {
                let quantDestinos = response.data.destination_addresses.length;
                let caminhos = [];
                let caminho = {};
                for (i = 0; i < quantDestinos; i++) {
                    caminho = {
                        origem: response.data.origin_addresses[0],
                        destino: response.data.destination_addresses[i],
                        duracao: response.data.rows[0].elements[i].duration_in_traffic.value,
                        distancia: response.data.rows[0].elements[i].distance.value
                    };
                    caminhos.push(caminho);
                }
                PesoAresta(caminhos, autonomia);
                gerarNos(caminhos);
            }
        )
        .catch((erro) => console.log(erro));
}

function gerarPreco() {
    return Math.random() * (4.7 - 5.06) + 5.06;
}


function PesoAresta(caminhos, autonomia) {
    let kmporlitro = document.getElementById('kmsporlitro').value;
    let peso = caminhos.map(caminho => {
        caminho.peso = ((caminho.distancia / 1000) / kmporlitro * gerarPreco() + (caminho.duracao / 60));
        return caminho.peso;
    });
    gerarNos(caminhos, peso, autonomia);
}


function gerarNos(caminhos, peso, autonomia) {
    console.log(caminhos);
    let nos = [];
    let origem = caminhos[0].origem;
    for (i = 0; i < caminhos.length; i++) {
        destinos = caminhos[i].destino;
        nos.push(destinos, origem);
    }

    let arestas = [];
    let aresta = {};
    for (i = 0; i < caminhos.length; i++) {
        aresta = {
            origem: caminhos[0].origem,
            destinos: caminhos[i].destino,
            pesos: caminhos[i].peso,
            distancia: caminhos[i].distancia
        };
        arestas.push(aresta);
    }
    console.log(arestas);

    let g = new Graph();
    let arrayNos = [...new Set(nos)];
    arrayNos.forEach(function (item) {
            g.addNode("'" + item + "'");
        }
    );

    arestas.forEach(function (item) {
        // console.log(item.distancia < autonomia);
        // if (item.distancia < autonomia) {
            //     console.log('tudo certo!');
            g.addEdge("'" + item.origem + "'", "'" + item.destinos + "'", "'" + item.pesos + "'");
        // }
    });

    console.log(g.floydWarshallAlgorithm());
}


class Graph {
    constructor() {
        this.edges = {};
        this.nodes = [];
    }

    addNode(node) {
        this.nodes.push(node);
        this.edges[node] = [];
    }

    addEdge(node1, node2, weight = 1) {
        this.edges[node1].push({node: node2, weight: weight});
        this.edges[node2].push({node: node1, weight: weight});
    }

    addDirectedEdge(node1, node2, weight = 1) {
        this.edges[node1].push({node: node2, weight: weight});
    }


    display() {
        let graph = "";
        this.nodes.forEach(node => {
            graph += node + "->" + this.edges[node].map(n => n.node).join(", ") + "\n";
        });
        console.log(graph);
    }

    floydWarshallAlgorithm() {
        let dist = {};
        for (let i = 0; i < this.nodes.length; i++) {
            dist[this.nodes[i]] = {};


            this.edges[this.nodes[i]].forEach(e => (dist[this.nodes[i]][e.node] = e.weight));

            this.nodes.forEach(n => {

                if (dist[this.nodes[i]][n] == undefined)
                    dist[this.nodes[i]][n] = Infinity;

                if (this.nodes[i] === n) dist[this.nodes[i]][n] = 0;
            });
        }

        this.nodes.forEach(i => {
            this.nodes.forEach(j => {
                this.nodes.forEach(k => {

                    if (dist[i][k] + dist[k][j] < dist[i][j])
                        dist[i][j] = dist[i][k] + dist[k][j];
                });
            });
        });
        return dist;
    }
}

let g = new Graph();
g.addNode("A"); //Pontos no grafo
g.addNode("B");
g.addNode("C");
g.addNode("D");

g.addEdge("A", "C", 100); //Arestas
g.addEdge("A", "B", 3);
g.addEdge("A", "D", 4);
g.addEdge("D", "C", 3);

console.log(g.floydWarshallAlgorithm());

//Pegando a localizacao do navegador
//Essa função está sendo usada apenas para exibir o local onde esta o usuario assim que se abre o site
$(document).ready(function () {
    if ('geolocation' in navigator) {
        //Verificando se o navegador suporta a geolocalizacao
        //Com o WatchPosition, ele verifica a localizacao continuamente
        const watcher = navigator.geolocation.watchPosition(function (position) {
            initMap(position.coords.latitude, position.coords.longitude); //Pegando a latitude e longitude do local onde esta o usuario
        }, function (error) {
            console.log(error)
        })
    } else {
        alert('ops! Não foi possível pegar a localização.') //Se o navegador nao suportar geolocalizacao, ele mostra essa mensagem
    }
});